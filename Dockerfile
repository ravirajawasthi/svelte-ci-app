# Build svelete app
FROM node:18.14-alpine as app_build
WORKDIR /app
COPY package.json .
RUN ["npm", "install"]
COPY . .
RUN ["npm", "run", "build"]

FROM node:18.14-buster-slim
COPY --from=app_build /app/build /prod
COPY --from=app_build /app/package-prod.json /prod/package.json
WORKDIR /prod
RUN apt-get update && apt-get install -y --no-install-recommends
ENV NODE_ENV production
USER node
ENTRYPOINT ["node", "/prod/index.js"]